package com.example.springboot.project.service;

import com.example.springboot.project.model.LoginDetails;
import com.example.springboot.project.model.Response;

public interface ServiceInterface {
	public Response RegisterDetails(LoginDetails input);
	public Response checkLoginDetails(String email, String password);
}
