package com.example.springboot.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.springboot.project.model.LoginDetails;

@Repository
public interface SportsRespository extends JpaRepository<LoginDetails, Integer>{
		
	@Query("from LoginDetails where email=:x and password=:y")
	LoginDetails findByIdAndPin(@Param("x") String email,@Param("y") String password);
}
