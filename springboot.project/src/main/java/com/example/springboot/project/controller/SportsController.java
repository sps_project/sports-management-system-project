package com.example.springboot.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.project.model.LoginDetails;
import com.example.springboot.project.model.Response;
import com.example.springboot.project.service.ServiceInterface;




@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class SportsController {
	
	@Autowired
	private ServiceInterface si;
	
	
	@PostMapping("/register")
	public Response RegisterDetails(@RequestBody LoginDetails input) {
       
		
		return si.RegisterDetails(input);
		}
 	@GetMapping("/login")
    public Response LoginDetails(@RequestParam("x") String email,@RequestParam("y") String password)
    {
 		return si.checkLoginDetails(email, password);
    }

}
