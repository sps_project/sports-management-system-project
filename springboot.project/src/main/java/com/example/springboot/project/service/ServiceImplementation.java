package com.example.springboot.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springboot.project.dao.SportsRespository;
import com.example.springboot.project.model.LoginDetails;
import com.example.springboot.project.model.Response;

@Service
public class ServiceImplementation implements ServiceInterface {
	
	@Autowired
	SportsRespository sr;

	@Override
	public Response RegisterDetails(LoginDetails input) {
		Response r = new Response(0,input,"failed");
		try {
			sr.save(input);
			r.setReason("Successful");
			r.setStatus(1);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("jpa error");
		}
		return r;
	}

	@Override
	public Response checkLoginDetails(String email, String password) {
		Response r = new Response(0,null,"failed");
		LoginDetails ld=sr.findByIdAndPin(email,password);
		if(ld!=null) {
			r.setStatus(1);
			r.setReason("Successful");
			r.setDetails(ld);
		}
		else
			System.out.println("not found");
		return r;
	}

}
