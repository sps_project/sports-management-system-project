package com.example.springboot.project.model;

public class Response {
	int status;
	LoginDetails input;
	String reason;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Response() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoginDetails getInput() {
		return input;
	}
	public void setDetails(LoginDetails input) {
		this.input = input;
	}
	public Response(int status, LoginDetails input, String reason) {
		super();
		this.status = status;
		this.input = input;
		this.reason = reason;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}
