import React, { Component } from "react";
import SportsService from "../services/SportsService";
import axios from 'axios';
import {backendUrl} from './constants';


export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
          input: {},
          errors: {}
        };
         
        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
      }
         
      handleChange(event) {
        let input = this.state.input;
        input[event.target.name] = event.target.value;
      
        this.setState({
          input
        });
      }
         
      handleLogin(event) {
        event.preventDefault();
      
        if(this.validate()){
            console.log(this.state);
      
            let input = {};
            input["email"] = "";
            input["password"] = "";
            this.setState({input:input});
      
            
        }

        const  input= this.state.input;
        axios.get(`${backendUrl}/api/v1/login/`,input)
        .then( (response) => {
            console.log(response);
            this.setState ({status:response.data.input.status});
            
        });  

        
      }
      
      validate(){
          let input = this.state.input;
          let errors = {};
          let isValid = true;
       
         
      
          if (!input["email"]) {
            isValid = false;
            errors["email"] = "Please enter your email Address.";
          }
      
          if (typeof input["email"] !== "undefined") {
              
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(input["email"])) {
              isValid = false;
              errors["email"] = "Please enter valid email address.";
            }
          }
      
          if (!input["password"]) {
            isValid = false;
            errors["password"] = "Please enter your password.";
          }
      
          
      
          if (typeof input["password"] !== "undefined") {
            if(input["password"].length < 6){
                isValid = false;
                errors["password"] = "Please add at least 6 charachter.";
            }
          }
      
          
      
          this.setState({
            errors: errors
          });
      
          return isValid;
      }

      componentDidMount(){
        SportsService.loginPlayer().then((res) => {
            this.setState({ input: res.data});
        });
    }
    render() {
        return (

            <div>
            <h3>Sign In</h3>
            <form onSubmit={this.handleLogin}>

            <div class="form-group">
            <label for="email">Email Address:</label>
            <input 
              type="text" 
              name="email" 
              value={this.state.input.email}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter email" 
              id="email" />
  
              <div className="text-danger">{this.state.errors.email}</div>
          </div>
  
          <div class="form-group">

            <label for="password">Password:</label>
            <input 
              type="password" 
              name="password" 
              value={this.state.input.password}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter password" 
              id="password" />
  
              <div className="text-danger">{this.state.errors.password}</div>
          </div>
          <input type="submit" value="Login" class="btn btn-success" />
        </form>
      </div>
    );
  }


}
                

                
