import axios from 'axios';

const PLAYER_API_BASE_URL = "http://localhost:7777/api/v1/register";
const PLAYER1_API_BASE_URL = "http://localhost:7777/api/v1/login";

class SportsService {

     registerPlayer(input){
         return axios.post(PLAYER_API_BASE_URL,input);
     }

     loginPlayer(input){
         return axios.get(PLAYER1_API_BASE_URL,input);
     }


    
}

 export default new SportsService()